// CS 392 Assignment 4
// I pledge my honor that I have abided by the Stevens Honor System. --Eric S. Londres

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

static int item1_counter = 0;
static int item2_counter = 0;
static int item3_counter = 0;

pthread_t threadID[3];
pthread_mutex_t thread_lock;



void * cs392_thread_run(void * ppath) {
  // given a file path, reads lines from the file and increments/decrements the global variables accordingly
  char * path = (char *) ppath;
  FILE * file = fopen(path, "r");
  char * line = NULL;
  size_t allocated_size = (size_t) NULL;

  while(getline(&line, &allocated_size, file) != -1) {
    pthread_mutex_lock(&thread_lock);
    switch(line[0]) {
    case '-':
      switch(line[5]) {
      case '1':
	--item1_counter;
	break;
      case '2':
	--item2_counter;
	break;
      case '3':
	--item3_counter;
	break;
      }
      break;
    case '+':
      switch(line[5]) {
      case '1':
	++item1_counter;
	break;
      case '2':
	++item2_counter;
	break;
      case '3':
	++item3_counter;
	break;
      }
      break;
    }
    pthread_mutex_unlock(&thread_lock);
  }
  return NULL;
}
    
  
int main(int argc, char ** argv) {  
  if(pthread_mutex_init(&thread_lock, NULL)) {
    perror("Mutex initialization failed\n");
    return 1;
  }
  for(int i = 0; i <= 2; ++i) {
    if(pthread_create(&(threadID[i]), NULL, &cs392_thread_run, (void *) argv[1])) {
      perror("Thread creation failed");
      return 2;
    }
  }
  for(int i = 0; i <= 2; ++i) {
    pthread_join(threadID[i], NULL);
  }
  pthread_mutex_destroy(&thread_lock);
  
  printf("The final value of item1_counter, item2_counter, and item3_counter are %d, %d, and %d\n",
	 item1_counter, item2_counter, item3_counter);
  
  return 0;  
}
