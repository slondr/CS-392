// I pledge my honor that I have abided by the Stevens Honor System. --Eric S. Londres


#include <stdio.h>
#include <signal.h>

#include "cs392_signal.h"

void handle_sigint() {
  fprintf(stderr, "Received SIGINT\n");
}

void handle_sigtstp() {
  fprintf(stderr, "Received SIGTSTP\n");
}

void signals() {
  MAKESIG(SIGINT, handle_sigint);
  MAKESIG(SIGTSTP, handle_sigtstp);
}
