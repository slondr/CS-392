// I pledge my honor that I have abided by the Stevens Honor System. --Eric S. Londres

#ifndef CS392_SIGNAL
#define CS392_SIGNAL

#define MAKESIG(SIGNL, HANDL) sigaction(SIGNL, &(struct sigaction) {.sa_handler = HANDL,.sa_flags = SA_RESTART}, NULL)

void handle_sigint();
void handle_sigtstp();
void signals();

#endif // CS392_SIGNAL
