// CS 392 Assignment 3: Shell
// I pledge my honor that I have abided by the Stevens Honor System. --Eric S. Londres

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "cs392_exec.h"
#include "cs392_log.h"
#include "cs392_signal.h"

int isempty(char * string) {
  for(int i = 0; i < strlen(string); ++i)
    if(!isspace((unsigned int *) string[i]))
      return 0;
  return 1;
}

void execute(char * command) {
  shell_log(command);
  if(!strcmp(command, "exit")) {
    exit(0);
  } else if(command[0] == 0xC) {
    // if we get a C-l, clear the screen
    proc_exec("clear");
  } else if(isempty(command)) {
    // only given whitespace, so we just skip the input
    return;
  } else {
    proc_exec(command);
  }
}

 
  
int main(void) {
  signals();
 repl:
  printf("cs392_shell $: ");

  char * user_input = 0;
  size_t line_length = 0;
  int char_count = getline(&user_input, &line_length, stdin);

  //fix getline not properly null-terminating input string
  user_input[char_count - 1] = 0;
  execute(user_input);

  free(user_input);
  goto repl;
}
