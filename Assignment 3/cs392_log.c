// I pledge my honor that I have abided by the Stevens Honor System. --Eric S. Londres

#include <stdio.h>

#include "cs392_log.h"

int shell_log(char * message) {
  FILE * logfile = fopen("cs392_shell.log", "a");
  fprintf(logfile, "%s\n", message);
  fflush(logfile);
  fclose(logfile);
  return 0;
}
