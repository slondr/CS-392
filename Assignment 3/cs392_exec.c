// CS 392 Assignment 3: Exec
// I pledge my honor that I have abided by the Stevens Honor System. --Eric S. Londres

#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include "cs392_exec.h"

int proc_exec(char * proc) {
  int proc_len = strlen(proc);
  pid_t pid = fork();
  if(pid == -1) { // error making child process
    perror("Fork error");
    return 1;
  } else if(pid == 0) { // I am a child
    // tokenize string    
    char * proc_argv[proc_len];
    int proc_argc = 0;
    char * env_buf = strtok(proc, " ");
    
    while(env_buf) {
      proc_argv[proc_argc++] = strdup(env_buf);
      env_buf = strtok(NULL, " ");
    }
    proc_argv[proc_argc] = NULL;
    free(env_buf);
    
    if(execvp(proc_argv[0], proc_argv) < 0) {
      perror("Execution error");
      exit(1);
    }

    exit(0);
  } else { // I am a parent
    int status;
    wait(&status);
  }
  return 0;
}
