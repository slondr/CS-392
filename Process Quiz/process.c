// CS 392 Process Quiz
// I pledge my honor that I have abided by the Stevens Honor System.
// -- Eric S. Londres

#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

struct sigaction act;

static void sigint_handler(int sig) {
  printf("Received SIGINT");
  act.sa_handler = SIG_DFL;
  sigaction(SIGINT, &act, 0);
}

int main() {
  // Register signal handler
  
  memset(&act, 0, sizeof(act));
  act.sa_handler = &sigint_handler;
  act.sa_flags = SA_RESETHAND;
  if(sigaction(SIGINT, &act, 0) < 0) {
    printf("egg");
    return 1;
  }
  while(1) {
    sleep(1);
    printf("Hellow\n");
  }
}

