// CS 392 Process Quiz
// I pledge my honor that I have abided by the Stevens Honor System.
// -- Eric S. Londres

#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

struct sigaction action;

static void sigint_handler(int sigt) {
  printf("Received SIGINT\n");
  action.sa_handler = SIG_DFL;
  sigaction(SIGINT, SIG_DFL, SIG_DFL);
}

int main() {
  // Register signal handler
  memset(&action, 0, sizeof(action));
  action.sa_handler = &sigint_handler;
  //action.sa_flags = SA_RESETHAND;
  if(sigaction(SIGINT, &action, SIG_DFL) == -1) {
    perror("egg");
    return 1;
  }
  while(1) {
    sleep(1);
    printf("Hellow\n");
  }
}

