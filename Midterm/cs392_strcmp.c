// I pledge my honor that I have abided by the Stevens Honor System. --Eric S. Londres
#include "cs392_midterm.h"

int cs392_strcmp(char * s1, char * s2) { 
  for(int i = 0;;++i) {
    if(s1[i] == 0 || s2[i] == 0) {
      if(s1[i] != 0) {
	return 1;
      } else if(s2[i] != 0) {
	return -1;
      } else {
	return 0;
      }
    }
    
    if(s1[i] > s2[i]) {
      return 1;
    } else if (s2[i] > s1[i]) {
      return -1;
    } else {
      continue;
    }
  }
}
    
