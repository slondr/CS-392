#ifndef MIDTERM
#define MIDTERM

char * cs392_strcpy(char * dest, char * src);
int cs392_strcmp(char * s1, char * s2);
char * cs392_strncat(char * dest, char * src, unsigned n);

#endif
