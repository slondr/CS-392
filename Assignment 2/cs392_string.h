// I pledge my honor that I have abided by the Stevens Honor System
//  -- Eric S. Londres
#ifndef CS392_STRING
#define CS392_STRING

void * cs392_memcpy(void * dst, void * src, unsigned n);
unsigned cs392_strlen(char * str);

#endif
