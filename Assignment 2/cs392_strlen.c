// I pledge my honor that I have abided by the Stevens Honor System.
//         -- Eric S. Londres

#include "cs392_string.h"

unsigned cs392_strlen(char * s) {
  unsigned length = 0;
  while(*(s++) != 0)
    ++length;
  return length;
}
