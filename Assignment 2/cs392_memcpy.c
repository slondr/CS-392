// I pledge my honor that I have abided by the Stevens Honor System.
//         --Eric S. Londres

void * cs392_memcpy(void * dst, void * src, unsigned num) {
  for(int i = 0; i < num; ++i) {
    *((char *) dst + i) = *((char *) src + i);
  }
}

