// CS 392 Assignment 5: Echo Server
// I pledge my honor that I have abided by the Stevens Honor System.
// --Eric S. Londres

#include "cs392_log.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(int argc, char ** argv) {
  if(argc != 2) { // error out if we weren't called correctly
    fprintf(stderr, "Usage: cs392_echoserver [portnumber]\n");
    return 1;
  }

  auto int port = atoi(argv[1]); // user supplies the port number
  if(port < 1) { // error out if the port wasn't given correctly
    fprintf(stderr, "Invalid port number.\n");
    return 2;
  }
  
  auto int socketd = socket(AF_INET, SOCK_STREAM, 0); // construct socket
  if(socketd == -1) { // if something went wrong, error out
    perror("Socket construction error");
    return 3;
  }

  // Now we construct the server by binding the socket to an ipv4 port:
  struct sockaddr_in server;
  server.sin_family = AF_INET; // IPv4
  server.sin_addr.s_addr = INADDR_ANY; // I don't care what I'm connected to as
  server.sin_port = htons(port); // user-supplied port, converted by htons()
  if(bind(socketd, (struct sockaddr *) &server, sizeof(server)) < 0) {
    perror("Bind error"); // something went wrong and we can't continue
    return 4;
  }

  listen(socketd, 5); // this can't fail if we made it this far

  while(1) { // loop this until killed
    // Now we work on accepting clients
    struct sockaddr_in client;
    socklen_t client_size = sizeof(client);
    int clientd = accept(socketd, (struct sockaddr *) &client, &client_size);
    if(clientd < 0) { // the client did something weird and it broke stuff
      perror("Accept error on client");
      return 5;
    }
    
    // at this point, a client has connected to us
    char * message_buffer = malloc(1024); // max size for a message is 1024
    if(read(clientd, message_buffer, 1024) < 0) { // reads from client
      perror("Error reading from socket");
      return 6; // if the client did something very strange, break
    }
    
    // we now have a valid message from the client, let's log it
    getpeername(clientd, (struct sockaddr *) &client, &client_size); // get IP
    cs392_socket_log(inet_ntoa(client.sin_addr), port); // calls log function
    // now we send the message back to the client
    if(write(clientd, message_buffer, strlen(message_buffer)) < 0) {
      perror("Error writing to client"); // something went wrong sending data
      return 7;
    }

    // now we clean up in preparation for the loop
    close(clientd);
    // done, restart
  }

  return 0;
}
