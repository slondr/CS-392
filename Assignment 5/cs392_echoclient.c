// CS 392 Echo Client
// I pledge my honor that I have abided by the Stevens Honor System.
//  -- Eric S. Londres

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(int argc, char ** argv) {
  if(argc != 3) { // error if we aren't called correctly
    fprintf(stderr, "Usage: cs392_echoclient [IP address] [Port number]\n");
    return 1;
  }

  auto int port = atoi(argv[2]); // user supplies the port number
  if(port < 1) { // error if the port doesn't make sense
    fprintf(stderr, "Invalid port number.\n");
    return 2;
  }

  auto int socketd = socket(AF_INET, SOCK_STREAM, 0); // construct socket
  if(socketd == -1) { // if something went wrong, error out
    perror("Socket construction error");
    return 3;
  }

  // now we make the client socket
  // most of this was taken from the class slides
  struct sockaddr_in echoserver;
  memset(&echoserver, 0, sizeof(echoserver)); // zero-out the struct
  echoserver.sin_family = AF_INET; // IPv4
  echoserver.sin_addr.s_addr = inet_addr(argv[1]); // user-supplied IP address
  echoserver.sin_port = htons(port); // user-supplied port
  if(connect(socketd, (struct sockaddr *) &echoserver, sizeof(echoserver)) < 0) {
    perror("Error connecting"); // error if something failed
    return 4;
  }

  // get message from user
  char * message = malloc(1024); // message can't be larger than 1024 bytes
  fgets(message, 1024, stdin); // read from stdin
  size_t length = strlen(message); // not foolproof, but good enough
  if(length < 1) { // error out if we didn't get data from stdin
    perror("Error reading from standard input");
    return 5;
  }

  // send message
  send(socketd, message, length, 0);

  // wait for response
  recv(socketd, message, length, 0);

  printf("%s", message); // uncomment this line if xu says we need to echo
  
  // we're done!
  return 0; 
}
