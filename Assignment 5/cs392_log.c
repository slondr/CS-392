// CS 392 Logger
// I pledge my honor that I have abided by the Stevens Honor System.
//  -- Eric S. Londres

#include "cs392_log.h"
#include <stdio.h>

void cs392_socket_log(char * ip, int port) {
  // accepts a string representing an IP address and an int representing a port
  // and writes them to a log file, creating it if it doesn't exist.
  FILE * file = fopen("cs392_socket.log", "a");
  fprintf(file, "%s %i\n", ip, port);
  fclose(file); // closes the file so the append actually works
}
