/* CS 392 Assignment 1 - sort.c
   Sorts a file full of integers. Written by Eric S. Londres
   I pledge my honor that I have abided by the Stevens Honor System. */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <ctype.h>
#include "sort.h"

int main(int argc, char ** argv) {
  if(argc != 3) {
    // too many or two few arguments were given
    fprintf(stderr, "Error: Two arguments expected but %i received.\n", argc-1);
    return 1;
  }
  // correct number of arguments was given
  FILE * inputfile;
  FILE * outputfile;
  char * inputfilename = argv[1];
  char * outputfilename = argv[2];
  char * buffer;
  struct stat size_check;
  inputfile = fopen(inputfilename, "r"); // open the file
  if(!inputfile) { // something went wrong with fopen
    perror("Error: Failed to open file");
    return 2;
  }
  /*
  stat(inputfilename, &size_check); // we have a good file, now we need memory
  int buf_size = size_check.st_size / sizeof(int);
  */
  fseek(inputfile, 0, SEEK_END);
  int buf_size = ftell(inputfile);
  rewind(inputfile);
  buffer = (char *) malloc(buf_size);
  if(!buffer) {
    // something failed with malloc, so we can't continue
    perror("Error: Memory allocation failure");
    return 3;
  }
  // we're good with our FD and memory allocation

  int * int_buffer = malloc(sizeof(int));
  int int_buffer_size = 1;
  char * line = 0;
  size_t line_width = 0;
  int ret = 2;
  while(ret > 0) {
    line_width = 0;
    ret = getline(&line, &line_width, inputfile);
    if(ret > 0) {
      int_buffer = realloc(int_buffer, int_buffer_size * sizeof(int));
      if(!int_buffer) {
	perror("Error");
	return 7;
      }
      int_buffer[int_buffer_size++ - 1] = strtol(line, 0, 0);
    } else {
      break;
    }
  }
  
  sort(&int_buffer, int_buffer_size - 1);

  if(fclose(inputfile)) {
    perror("Error: Failed to close file");
    return 4;
  }
  // now we write out to the output file, truncating the file in the process
  outputfile = fopen(outputfilename, "w");
  // write(outputfile, int_buffer, int_buffer_size);
  for(int i = 1; i < int_buffer_size; ++i) {
    fprintf(outputfile, "%i\n", int_buffer[i]);
  }
      
  

  
  if(fclose(outputfile)) {
    perror("Error: Failed to close file");
    return 4;
  }
  // everything went good, so we exit normally
  free(buffer);
  return 0;
}
