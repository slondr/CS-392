/* CS 392 Assignment 1 - cat.c
   A rudimentary implementation of cat, by Eric S. Londres
   I pledge my honor that I have abided by the Stevens Honor System. */

#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>

int cat(FILE * file, char * buffer, size_t buffer_size) {
  /* cat() takes a file pointer, pre-allocated memory space, and amount of bytes
     to transfer, and then reads the number of bytes to transfer from the file
     into the pre-allocated memory space. Finally, it prints the resulting
     buffer space to stdout. */
  if(fread(buffer, 1, buffer_size, file) != buffer_size) {
    if(!ferror(file)) {
      perror("Error reading from file");
      return 1;
    } else {
      fprintf(stderr, "Unknown error. Terminating.\n");
      return 2;
    }
  } else {
    // reading the file was successful
    rewind(file);
    char buf_char; // initialize buffer character
    while((buf_char = fgetc(file)) != EOF) { 
      printf("%c", buf_char); 
    } 
  }
  // everything went well, so we can exit successfully
  return 0;
}



int main(int argc, char ** argv) {
  if(argc != 2) {
    fprintf(stderr,
	    "Error: One argument expected but %i received.\n", argc - 1);
    return 1;
  } 
  // correct number of arguments was given
  FILE * file;
  char * filename = argv[1];
  char * buffer;
  struct stat size_check;
  file = fopen(filename, "r"); // open the file
  if(!file) { // something went wrong. probably a bad filename
    perror("Error: Failed to open file");
    return 3;
  }
  stat(filename, &size_check); // now we need to allocate memory
  buffer = (char *)malloc(size_check.st_size + 1);
  if(!buffer) { // if memory allocation failed, we can't continue
    perror("Error: Memory allocation failure");
    return 2;
  }
  // correct amount of memory was successfully allocated. we are good to go
  if(cat(file, buffer, size_check.st_size)) {
    perror("Error");
    return 4;
  }
  // clean up memory & close open FDs
  if(fclose(file)) {
    perror("Error: Failed to close file");
    return 5;
  }
  free(buffer);
  // everything was successful and we exit normally
  return 0;
}
