#pragma once
#ifndef T_SORT
#define T_SORT

#define NODEBUG
#define DEBUG4

int parse(FILE * file, char * buffer, size_t buffer_size) {
  /* Takes a file pointer, a pointer to pre-allocated memory space, and an
     integer amount of bytes to transfer. Reads the number of bytes given from
     the file given and stores the result in the buffer given. */
  if(!fread(buffer, 1, buffer_size, file) != buffer_size) {
    if(ferror(file)) {
      perror("Error reading from file");
      return 1;
    }
#ifdef DEBUG
    printf("DEBUG\n%sEND DEBUG", buffer);
#endif
  }
  // reading the file was successful
  rewind(file);
  return 0;
}



int * convert(char * buffer, int buffer_size) { 
  char * nptr, * str = buffer;
  int * int_buf = (int *) malloc(buffer_size);
  int index = 0;
  do {
    if(isdigit(*str)) {
      int_buf[index++] = strtol(str, &nptr, 10);
    } else {
      nptr = str + 1;
    }
    str = nptr;
  } while(*str);
  return int_buf;
}

void swap(int ** array, int i, int j) {
  int k = *array[i];
  *array[i] = *array[j];
  *array[j] = k;
}

void sort(int ** buffer, int length) {
#ifdef DEBUG2
  printf("DEBUG\n");
  for(int i = 0; i <= length; ++i) {
    printf("%i ", (*buffer)[i]);
  }
  printf("\nEND DEBUG\n");
#endif
  length++;
  for(int i = 0; i < length; i++) {
    for(int j = i; j > 0 && (*buffer)[j - 1] > (*buffer)[j]; j--) {
      int k = (*buffer)[j];
      (*buffer)[j] = (*buffer)[j - 1];
      (*buffer)[j - 1] = k;
      
    }
  }
  

#ifdef DEBUG2
  printf("DEBUG\n");
  for(int i = 0; i <= length; ++i) {
    printf("%i ", (*buffer)[i]);
  }
  printf("\nEND DEBUG\n");
#endif


  
}

int write(FILE * file, int * buf, int buf_size) {
  for(int i = 0; i < buf_size; ++i) {
    char * print_value;
    int length = sprintf(print_value, "%i\n", buf[i]);
    fwrite(print_value, buf_size / 2, length, file);
  }
}


#endif // T_SORT
