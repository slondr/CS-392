// thread in-class practice

#include <stdio.h>
#include <pthread.h>

volatile counter = 0;

void * run_thread(void * noarg) {
  for(int i = 0; i < 1000; ++i)
    ++counter;
  pthread_exit(NULL);
  return NULL;
}

int main(void) {
  pthread_t t1, t2;
  pthread_create(&t1, NULL, run_thread, NULL);
  pthread_create(&t2, NULL, run_thread, NULL);
  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
  printf("Counter := %i\n", counter);
}
